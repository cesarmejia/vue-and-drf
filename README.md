# Vue And DRF

<!-- TOC -->

- [Vue And DRF](#vue-and-drf)
  - [Outline](#outline)

<!-- /TOC -->

## Outline

- [ ] Introduction
- [ ] Overview
- [ ] Setting Up Dev Environment
- [ ] Section Objectives
- [ ] APIs, JSON, Endpoints
- [ ] REST, HTTP, and Status Codes
- [ ] The requirements module
- [ ] Django API, part one
- [ ] Django API, part two
- [ ] Compentency Assessment - Project Introduction
- [ ] Compentency Assessment - Project Solution
- [ ] DRF Level One - Section Objectives
- [ ] Intro to DRF and NewsAPI Project Setup
- [ ] What are serializers?
- [ ] @api_view Decorator - part one
- [ ] @api_view Decorator - part two
- [ ] The APIView Class
- [ ] Serializers Validation
- [ ] The ModelSerializer Class
- [ ] How to handle nested relationships
- [ ] DRF Level One - Compentency Assessment - JobBoardAPI Project Introduction
- [ ] DRF Level One - Compentency Assessment - JobBoardAPI Project Solution
- [ ] DRF Level Two - Section Objectives
- [ ] EbooksAPI - Project Setup
